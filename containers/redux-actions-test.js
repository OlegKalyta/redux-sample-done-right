import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, combineReducers, compose  } from 'redux';
import { createAction, handleAction, handleActions } from 'redux-actions';

import {todos, visibilityFilter } from '../actions/TodoActions';
import TodoApp from '../components/Todo';

import { devTools, persistState } from 'redux-devtools';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
import DiffMonitor from 'redux-devtools-diff-monitor';

import React, { Component, PropTypes } from 'react';


// ACTIONS & REDUCERS

const increment = createAction('INCREMENT');
const decrement = createAction('DECREMENT');
const incrementObject = createAction('INCREMENT_OBJECT');

const reducer = handleActions({
    INCREMENT: (state, action) => ({
        counter: state.counter + action.payload
    }),

    DECREMENT: (state, action) => ({
        counter: state.counter - action.payload
    }),

    INCREMENT_OBJECT: (state, action) => ({
        counter: state.counter + action.payload.a
    })
}, {counter: 0});

// APP

const finalCreateStore = compose(
    devTools(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
)(createStore);


const store = finalCreateStore(reducer);

const render = () => {
    ReactDOM.render(
        <div>
            <Provider store={store}>
                <TestApp store={store}/>
            </Provider>
            <DebugPanel top right bottom>
                <DevTools store={store}
                          monitor={DiffMonitor}
                          visibleOnLoad={true}/>
            </DebugPanel>
        </div>,
        document.getElementById('app')
    );
};

// VIEW

class TestApp extends Component {
    render() {
        return (
            <div>
                <button onClick={() => { this.props.store.dispatch(
                                            incrementObject({a: 'i am a!'}));
                                       }}>
                    Push ME!
                </button>

                <h2>{this.props.store.getState().counter}</h2>
                <button onClick={() => {this.props.store.dispatch(increment(1));}}>
                    +
                </button>
                <button onClick={() => {this.props.store.dispatch(decrement(1));}}>
                    -
                </button>
            </div>
        );
    }
}


store.subscribe(render);
render();
