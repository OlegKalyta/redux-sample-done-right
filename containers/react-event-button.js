import ReactDOM from 'react-dom';
import React from 'react';

import { Provider } from 'react-redux';
import { createStore, combineReducers, compose  } from 'redux';
import { createAction, handleAction, handleActions } from 'redux-actions';

import { devTools, persistState } from 'redux-devtools';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
import DiffMonitor from 'redux-devtools-diff-monitor';

// ACTIONS & REDUCER
const increment = createAction('INCREMENT');

export const reducer = handleActions({
    INCREMENT: (state, action) => ({
        payload: {v: action.payload.v++}
    }),

}, {id: 0, v: 0});


// APP
const finalCreateStore = compose(
    devTools(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
)(createStore);

const store = finalCreateStore(reducer);

class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {counter: 0};
    }

    addOne = () => {
        var {state: {counter}} = this;

        this.setState({counter: counter + 1});
    }

    render() {
        return (
            <li onClick={() => {this.addOne(); this.props.addOne(); }}>
                {this.props.l} {this.state.counter}
            </li>
        );
    }
}


// JSX
class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {counter: 0};
    }

    addOne = () => {
        var {state: {counter}} = this;
        this.setState({counter: counter + 1});
    }

    render() {
        const {props: {list}, addOne} = this;

        return (
            <div>
                <ul>
                    {list.map(l =>
                        <ListItem key={l} l={l} addOne={addOne}/>
                    )}
                </ul>

                {this.state.counter}
            </div>
        );
    }
}

class Parent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {counter: 0};
    }

    addOne = () => {
        var {state: {counter}} = this;
        this.setState({counter: counter + 1});
    }

    render() {
        const {props: {list}, addOne} = this;
        return (
            <div>
                <Counter list={list} f={addOne}/>
            </div>);
    }
}


const render = () => {
    ReactDOM.render(
        <div>
            <Provider store={store}>
                <Parent list={[100, 200, 300]} f={(v) => console.log(v) }/>
            </Provider>
            <DebugPanel top right bottom>
                <DevTools store={store}
                          monitor={DiffMonitor}
                          visibleOnLoad={true}/>
            </DebugPanel>
        </div>,
        document.getElementById('app')
    );
};

render();
//
//<div>
//    <input
//        type="text"
//        placeholder="Your name"
//        value={this.state.author}
//        onChange={this.handleAuthorChange}
//    />
//</div>
//<button onClick={console.log(this.state.author)}>Get Value From State</button>
