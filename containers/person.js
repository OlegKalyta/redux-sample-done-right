var person = Object.create(null);

function get_full_name() {
    return this.first_name;
}

function set_full_name(new_name) {
    this.first_name = new_name;
    if (window.forceUpdate) {
        window.forceUpdate();
    }
}

Object.defineProperty(person, 'name', {
    get: get_full_name
    , set: set_full_name
    , configurable: true
    , enumerable: true
});

function get_age() {
    return this.age_inner;
}

function set_age(new_age) {
    this.age_inner = new_age;
    if (window.forceUpdate) {
        window.forceUpdate();
    }
}


Object.defineProperty(person, 'age', {
    get: get_age
    , set: set_age
    , configurable: true
    , enumerable: true
});

person.name = 'Oleg';
person.age = '27';

export default person;
