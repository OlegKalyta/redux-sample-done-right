import ReactDOM from 'react-dom';
import React from 'react';

import { Provider } from 'react-redux';
import { reduxForm, reducer as formReducer} from 'redux-form';
import { createStore, combineReducers, compose  } from 'redux';


import {todos, visibilityFilter } from '../actions/TodoActions';
import TodoApp from '../components/Todo';

import { devTools, persistState } from 'redux-devtools';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
import DiffMonitor from 'redux-devtools-diff-monitor';

const reducers = {
    todos,
    visibilityFilter,
    form: formReducer.plugin({
        simple: (state, action) => {
            console.log(action);    // analogue of dev-tools for this form
            switch (action.type) {
                case 'CLEAR_TODO':
                    console.log(1);
                    return {
                        ...state,
                        todoField: {}
                    };
                default:
                    return state;
            }
        }
    })
};


const finalCreateStore = compose(
    devTools(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
)(createStore);

const reducer = combineReducers(reducers);

const store = finalCreateStore(reducer);

const render = () => {
    ReactDOM.render(
        <div>
            <Provider store={store}>
                <TodoApp store={store} todos={store.getState().todos}/>
            </Provider>
            <DebugPanel top right bottom>
                <DevTools store={store}
                          monitor={DiffMonitor}
                          visibleOnLoad={true}/>
            </DebugPanel>
        </div>,
        document.getElementById('app')
    );
};

store.subscribe(render);
render();

window.store = store;
