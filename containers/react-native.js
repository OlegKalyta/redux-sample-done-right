import ReactDOM from 'react-dom';
import React from 'react';

import { Provider } from 'react-redux';
import { createStore, combineReducers, compose  } from 'redux';
import { createAction, handleAction, handleActions } from 'redux-actions';

import { devTools, persistState } from 'redux-devtools';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
import DiffMonitor from 'redux-devtools-diff-monitor';

// ACTIONS & REDUCER
const increment = createAction('INCREMENT');

const reducer = handleActions({
    INCREMENT: (state, action) => ({
        payload: {v: action.payload.v++}
    }),

}, {id: 0, v: 0});


// APP
const finalCreateStore = compose(
    devTools(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
)(createStore);

const store = finalCreateStore(reducer);


// JSX
var ListUL = React.createClass ({
    render: function () {
        return (
            <div>
                <ul>
                    {this.props.list.map(l =>
                        <li key={l}
                            onClick={() => this.props.f(l)}>
                            {l}
                        </li>
                    )}
                </ul>
            </div>);
    }
});


var ListOL = React.createClass ({
    render: function () {
        return (
            <div>
                <ol>
                    {this.props.list.map(l =>
                        <li key={l}
                            onClick={() => this.props.f(l)}>
                            {l}
                        </li>
                    )}
                </ol>
            </div>);
    }
});

const Parent = React.createClass ({
    render: function () {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
});

// OPTION 1
//<Parent>
//    <ListOL list={[100, 200, 300]} f={(v) => console.log(v)} />
//</Parent>



// OPTION 2

function Wrapped(Component) {
    return React.createClass({
        render: function() {
            return (
                <div className="wrapper">
                    <Component {...this.props}/>
                </div>
            );
        }
    });
};


var WrappedList = Wrapped(ListOL);


const render = () => {
    ReactDOM.render(
        <div>
            <Provider store={store}>
                <WrappedList list={[100, 200, 300]} f={(v) => console.log(v)}/>
            </Provider>
            <DebugPanel top right bottom>
                <DevTools store={store}
                          monitor={DiffMonitor}
                          visibleOnLoad={true}/>
            </DebugPanel>
        </div>,
        document.getElementById('app')
    );
};

render();
