import ReactDOM from 'react-dom';
import React, {Component} from 'react';
import person from './person';
window.aaaa = person;

class Parent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.forceUpdate = this.forceUpdate.bind(this);
    }

    //shouldComponentUpdate(nextProps) {
    //    return JSON.stringify(this.props.obj) === JSON.stringify(nextProps.obj);
    //}

    render() {
        console.log('render  parent');
        return (
            <div className="wrapper">
                <Cont {...this.props} />
                <Cont {...this.props} forceOnSecondField={true}/>
            </div>
        );
    }
}

class Cont extends React.Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        if (this.props.forceOnSecondField) {
            return this.props.obj.age !== nextProps.obj.age;
        }
        else return this.props.obj.name !== nextProps.obj.name;
    }

    render() {
        console.log('render child');
        var prop = this.props.forceOnSecondField ? this.props.obj.age : this.props.obj.name;
        return (
            <div>
                <h3>{prop}</h3>
            </div>
        );
    }
}

const render = () => {
    ReactDOM.render(
        <Parent obj={window.aaaa}>

        </Parent>,
        document.getElementById('app')
    );
};

render();
