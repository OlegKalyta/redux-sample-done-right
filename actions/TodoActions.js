import { ADD_TODO, TOGGLE_TODO, SET_VISIBILITY_FILTER } from '../constants/ActionTypes';
import { createAction, handleAction, handleActions } from 'redux-actions';

const todo = handleActions({
    ADD_TODO: (state, action) => ({
        id: action.payload.id,
        text: action.payload.text,
        completed: false
    }),

    TOGGLE_TODO: (state, action) => (state.id !== action.payload.id) ? state : Object.assign({}, state, {completed: !state.completed})

}, {id: 0, text: '', completed: false});

export const todos = handleActions({
    ADD_TODO: (state, action) => [...state, todo({}, action)],

    TOGGLE_TODO: (state, action) => state.map(t => todo(t, action))

}, []);


export const visibilityFilter = (state = 'SHOW_ALL', action) => {
    switch (action.type) {
        case SET_VISIBILITY_FILTER:
            return action.filter;
        default:
            return state;
    }
};
