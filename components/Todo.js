import React, { Component, PropTypes } from 'react';
import { ADD_TODO, TOGGLE_TODO } from '../constants/ActionTypes';

import {reduxForm} from 'redux-form';
import { createAction, handleAction, handleActions } from 'redux-actions';
export const fields = ['todoField'];

let nextTodoId = 0;


let addAction = createAction(ADD_TODO);
let toggleAction = createAction(TOGGLE_TODO);


const validate = values => {
    const errors = {};
    if (!values.todoField) {
        errors.todoField = 'Required';
    } else if (values.todoField.length < 3) {
        errors.todoField = 'Must be 3 characters or more';
    }
    return errors;
};

class TodoApp extends Component {
    static propTypes = {
        fields: PropTypes.object.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        resetForm: PropTypes.func.isRequired,
        submitting: PropTypes.bool.isRequired
    };

    render() {
        const { fields: {todoField}} = this.props;
        var val = '';
        if (this.props.store.getState().form.simple.todoField) {
            val = this.props.store.getState().form.simple.todoField.value;
        }


        return (
            <div>
                <input ref={node => {
                    this.input = node;
                }} {...todoField}/>


                {val}
                {todoField.error && todoField.touched && <div>{todoField.error}</div>}
                <button onClick={() => {
            this.props.store.dispatch(addAction(
                {
                    text: todoField.value,
                    id: nextTodoId++
                }));
            //this.props.store.dispatch({"type":"redux-form/CHANGE","field":"todoField","value":"","touch":false,"form":"simple"});
            this.props.store.dispatch({type: 'CLEAR_TODO'});

        }}>
                    Add Todo
                </button>

                <ul>
                    {this.props.todos.map(todo =>
                        <li key={todo.id}
                            onClick={() => {
            this.props.store.dispatch(toggleAction({id: todo.id}));
        }}
                            style={{
            textDecoration: todo.completed ?
                'line-through' :
                'none'
        }}>
                            {todo.text}
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}


export default reduxForm({
    form: 'simple',
    fields,
    validate
})(TodoApp);


